#!/usr/bin/env groovy

def call(args) {
    println "Class: " + args.getClass().getName()
    println "Body: " + args
    println "Name 1: " + args.name1
    println "Name 2: " + args.name2
}
